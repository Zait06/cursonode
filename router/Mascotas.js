const express = require('express');
const router = express.Router();

const Mascota = require('../models/Mascota')

router.get('/',async (req, res) => {

    try {
        arrayMascotasDB = await Mascota.find();
        // console.log(arrayMascotasDB);
        res.render("mascotas",{
            // arrayMascotas: [
            //     {
            //         id: 'asdf',
            //         nombre: 'Rex',
            //         descripcion: 'Rex description'
            //     },
            //     {
            //         id: 'qwer',
            //         nombre: 'Chanchan',
            //         descripcion: 'Desripcion de Chanchan'
            //     }
            // ]
            arrayMascotas: arrayMascotasDB
        })
    } catch (error) {
        console.log(error)
    }
})

router.get("/crear",(req, res)=>{
    res.render("crear");
});

router.post("/", async(req, res)=>{
    const body = req.body;
    console.log(body);
    try {
        // const mascotaDB = new Mascota(body);
        // await mascotaDB.save();
        await Mascota.create(body);
        res.redirect('/mascotas');
    } catch (error) {
        console.log(error);
    }
})

router.get("/:id", async(req,res)=>{
    const id = req.params.id;
    try {
        const mascotaDB = await Mascota.findOne({_id: id});
        res.render('detalle',{
            mascota: mascotaDB,
            error: false
        })
    } catch (error) {
        res.render('detalle',{
            error: true,
            mensaje: "No se encuentra la mascota"
        })
    }
})

router.delete("/:id", async(req,res)=>{
    const id = req.params.id;
    try {
        const mascotaDB = await Mascota.findByIdAndDelete({_id: id});
        if(mascotaDB){
            res.json({
                estado: true,
                mensaje: "Eliminado"
            })
        }else{
            res.json({
                estado: false,
                mensaje: "Algo salio mal"
            })
        }
    } catch (error) {
        console.log(error)
    }
})

router.put('/:id',async(req, res) => {
    const id = req.params.id;
    const body = req.body;
    try {
        const mascotaDB = await Mascota.findByIdAndUpdate(
            id,
            body,
            {useFindAndModify: false}
        )
        console.log(mascotaDB);
        res.json({
            estado: true,
            mensaje: "Editado"
        })
    } catch (error) {
        res.json({
            estado: false,
            mensaje:"Algo anda mal"
        })
    }
})

module.exports = router;