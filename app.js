const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// Parse application/json
app.use(bodyParser.json());

require('dotenv').config()

const port = process.env.PORT || 3000;

// Conexión a base de datos
const mongoose = require('mongoose');
const USER='veterinario';
const PASSWORD='veterinario123';
const DBNAME='veterinaria';
const uri = `mongodb://${USER}:${PASSWORD}@127.0.0.1:27017/${DBNAME}`;

mongoose.connect(uri,
    {useNewUrlParser: true, useUnifiedTopology: true}
).then(
        () => console.log('Base de datos conectada')
    ).catch(
        e => console.log(e)
    );

// Motor de plantillas
app.set('view engine', 'ejs');
app.set('views',__dirname + '/views');

// Middleware
app.use(express.static(__dirname + '/public'));

// Rutas de la API
app.use('/',require('./router/RutasWeb'));
app.use('/mascotas',require('./router/Mascotas'));

app.use((req,res,next) => {
    res.status(404).render('404',{
        titulo: 404,
        descripcion: "Títutlo del sitio web"
    })
})

app.listen(port,() => {
    console.log("Servidor en servicio en el puerto ",port)
})

// const http = require("http")

// // Se crea un servicor (requerimiento, respuesta)
// const server = http.createServer((req, res)=>{
//     res.end("Respuesta a la solicitud v.3") // Agregalo al final
// });

// const puerto = 1234;

// server.listen(puerto,()=>{
//     console.log("Escuchando...")
// })